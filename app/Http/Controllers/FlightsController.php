<?php

namespace App\Http\Controllers;

use App\Library\IvaoWhazzup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FlightsController extends Controller
{

    public function index() {
        $departures = DB::table('inouttfc')->where('departure', 'like', 'LK%')->get();
        $arrivals = DB::table('inouttfc')->where('arrival', 'like', 'LK%')->get();
        $atc = DB::table('atconline')->select(['position', 'freq'])->get();
        return view("flights")->with(compact('departures', 'arrivals', 'atc'));
    }

}
