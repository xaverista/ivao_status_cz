<?php


namespace App\Library;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class IvaoWhazzup
{

    private $filename;
    private $disk;

    public function __construct($filename)
    {
        $this->disk = App::environment('local') ? 'local' : 'datafeed';
        $this->filename = $filename;
    }

    public function exists()
    {
        return Storage::disk($this->disk)->exists($this->filename);
    }

    public function getClients()
    {
        $whazzup = Storage::disk($this->disk)->get($this->filename);
        $whazzup = Str::of($whazzup)->explode("\n");

        // Initialize variables
        $clients = [];
        $actualType = "";

        foreach ($whazzup as $item) {
            if (Str::startsWith($item, "!")) {
                $actualType = Str::of($item)->ltrim('!');
                continue;
            }

            if ($actualType == "CLIENTS") {
                $tmpClient = [];
                $client = Str::of($item)->explode(":");
                // Fill client info
                $tmpClient['callsign'] = $client[0];
                $tmpClient['vid'] = $client[1];
                $tmpClient['name'] = $client[2];
                $tmpClient['type'] = Str::lower($client[3]);
                $tmpClient['rating'] = $client[41];
                $tmpClient['connTime'] = $client[37];
                $tmpClient['softwareName'] = $client[38];
                $tmpClient['softwareVersion'] = $client[39];

                if ($tmpClient['type'] == "atc") {
                    $tmpClient['facilityType'] = $client[18];

                    if ($tmpClient['facilityType'] != "0") {
                        $tmpClient['frequency'] = $client[4];
                        $tmpClient['atis'] = $client[35];
                    }
                }

                if ($tmpClient['type'] == "pilot") {
                    $aircraft = Str::of($client[9])->explode("/");
                    $tmpClient['aircraft'] = count($aircraft) > 1 ? $aircraft[1] : "UNKW";
                    $tmpClient['altitude'] = $client[7];
                    $tmpClient['groundSpeed'] = $client[8];
                    $tmpClient['heading'] = $client[45];
                    $tmpClient['squawk'] = $client[17];
                    $tmpClient['origin'] = $client[11];
                    $tmpClient['destination'] = $client[13];
                }

                $clients[] = $tmpClient;
            }

        }

        return $clients;
    }

    public function getBySpecific($key, $search = "", $type = null)
    {
        $collection = collect($this->getClients());
        $filtered = $collection->filter(function ($item) use ($key, $search, $type) {
            if ($item['type'] != $type && $type !== null) {
                return false;
            }
            return Str::startsWith($item[$key], $search);
        });

        return $filtered;
    }

}
