<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Lukas Novotny">
    <title>IVAO LKAA Flights</title>
    <link rel="stylesheet" href="{{ asset("assets/css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/app.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/fawsm-all.min.css") }}">
</head>
<body>
<div class="container-fluid">
    <div class="row pt-5">
        <div class="col">
            <h1 class="text-white text-uppercase text-center">LKAA flights</h1>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-lg-6">
            <div class="card card-flights">
                <div class="card-header">
                    <i class="fas fa-plane-departure icon"></i> Odlety / Departures
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table flight-list">
                            <tr class="flight-row header font-weight-bold">
                                <td>Callsign</td>
                                <td>Origin</td>
                                <td>To/Via</td>
                            </tr>
                            @forelse($departures as $flight)
                                <tr class="flight-row">
                                    <td>{{ $flight->callsign }}</td>
                                    <td>{{ $flight->departure }}</td>
                                    <td>{{ $flight->arrival }}</td>
                                </tr>
                            @empty
                                <tr class="flight-row font-weight-bold">
                                    <td colspan="5" class="text-center text-danger">NO DEPARTURES AT THE MOMENT</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-flights">
                <div class="card-header">
                    <i class="fas fa-plane-arrival icon"></i> Přílety / Arrivals
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table flight-list">
                            <tr class="flight-row header font-weight-bold">
                                <td>Callsign</td>
                                <td>Origin</td>
                                <td>Destination</td>
                            </tr>
                            @forelse($arrivals as $flight)
                                <tr class="flight-row">
                                    <td>{{ $flight->callsign }}</td>
                                    <td>{{ $flight->departure }}</td>
                                    <td>{{ $flight->arrival }}</td>
                                </tr>
                                @empty
                                    <tr class="flight-row font-weight-bold">
                                        <td colspan="5" class="text-center text-danger">NO ARRIVALS AT THE MOMENT</td>
                                    </tr>
                                @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row py-5">
        <div class="col-lg-6 offset-lg-3">
            <div class="card card-flights">
                <div class="card-header">
                    <i class="fas fa-headphones-alt icon"></i> Online ATC
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table flight-list">
                            <tr class="flight-row header font-weight-bold">
                                <td>Callsign</td>
                                <td>Frequency</td>
                            </tr>
                            @forelse($atc as $controller)
                                <tr class="flight-row">
                                    <td>{{ $controller->position }}</td>
                                    <td>{{ $controller->freq }}</td>
                                </tr>
                            @empty
                                <tr class="flight-row font-weight-bold">
                                    <td colspan="3" class="text-center text-danger">NO ATC AVAILABLE</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
